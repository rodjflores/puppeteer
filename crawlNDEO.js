const puppeteer = require('puppeteer');
const csv = require('csv-parser');
var fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    var urlArray = fs.readFileSync('LinksToSchools.rtf', 'utf8').split('\n');

    schools = [];
    x = 0
    for (let url of urlArray) {

        await page.goto(url);
        const school = {};
        schoolName = await page.$$eval('h1', h1s => h1s.map(a => a.innerHTML));
        schoolName =  schoolName[0].substring(0,schoolName[0].indexOf('<'));
        contact = await page.$$eval(".detail-contact > .row-detail", x => x.map(a => a.innerHTML));
    
        address = await page.$$eval(".row-detail > .address-container", x => x.map(a => a.innerHTML));
        address = address[0].replace(/\n/ig,"");
        address = address.replace(/\t/ig, "");
        address = address.replace(/<br>/ig, ", ");
        
        phone = await page.$$eval(".detail-phone > .row-detail > a", x => x.map(a => a.innerHTML));
        email = await page.$$eval(".detail-email > .row-detail > a", x => x.map(a => a.innerHTML));
        website = await page.$$eval(".detail-website > .row-detail > a", x => x.map(a => a.innerHTML));
    
    
        schoolName.length > 1 ? school.schoolName = schoolName : school.schoolName = '-' ;
        contact[0] != undefined > 1 ? school.contact = contact[0] : school.contact = '-' ;
        address.length > 1 ? school.address = address : school.address = '-' ;
        phone[0] != undefined > 1 ? school.phone = phone[0] : school.phone = '-' ;
        email[0] != undefined > 1 ? school.email = email[0] : school.email = '-' ;
        website[0] != undefined > 1 ? school.website = website[0] : school.website = '-';
        
        schools.push(school);
        console.log(x)
        x++
    }


    
    const csvWriter = createCsvWriter({
        path: 'output/Schools.csv',
        header: [
            { id: 'schoolName', title: 'School Name' },
            { id: 'contact', title: 'Contact' },
            { id: 'address', title: 'Address' },
            { id: 'phone', title: 'Phone' },
            { id: 'email', title: 'Email' },
            { id: 'website', title: 'Website' },
        ]
    });

    csvWriter
        .writeRecords(schools)
        .then(() => console.log('DONE!!!'));

    
    await browser.close();
})();


