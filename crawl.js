const puppeteer = require('puppeteer');
const csv = require('csv-parser');
var fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    var urlArray = fs.readFileSync('LinksToResellerData.rtf', 'utf8').split('\n');

    x = 0
    
    companies = [];

    for (let url of urlArray) {
            await page.goto(url);
            const company = {};          
            const h1 = await page.$$eval('h1', h1s => h1s.map(a => a.innerHTML));
            company.name = h1[0].replace(/(<([^>]+)>)/ig,""); //TODO: Strip out <span>
        
            const rows = await page.$$eval('.ifmobt > tbody > tr', trs => trs.map(a => a.children[a.children.length-1].innerHTML));
        
            company.phoneNumber = rows[4];
            company.emailAddress = rows[5].replace(/(<([^>]+)>)/ig,"");
            company.websiteUrl = rows[6].replace(/(<([^>]+)>)/ig,"");
        
            companies.push(company);
            console.log(x);
            x++;
    }
    
    const csvWriter = createCsvWriter({
        path: 'output/Companies.csv',
        header: [
          {id: 'name', title: 'Name'},
          {id: 'phoneNumber', title: 'Phone'},
          {id: 'emailAddress', title: 'Email'},
          {id: 'websiteUrl', title: 'URL'},
        ]
    });

    
    csvWriter
    .writeRecords(companies)
        .then(() => console.log('DONE!!!'));
    
    await browser.close();
})();


